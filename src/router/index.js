import Vue from 'vue'
import VueRouter from 'vue-router'
import Home from '../views/Home.vue'

// 解决ElementUI导航栏中的vue-router在3.0版本以上重复点菜单报错问题
const originalPush = VueRouter.prototype.push
VueRouter.prototype.push = function push(location) {
  return originalPush.call(this, location).catch(err => err)
}

Vue.use(VueRouter)

const Son = () => import('../views/Son')

const routes = [
    /** 默认跳转的路由 */
  {
    path: '/',
    redirect: '/home'
  },
  {
    path: '/home',
    name: `Home`,
    meta: {
      title: `首页`,
      keepAlive: true
    },
    component: Home,
    children: [
      {
        /** 注意：子路由的path前缀不加左斜杠会继承父路由的path */
        path: 'son1',
        meta: {
          title: `嵌套子组件`,
        },
        component: Son
      },
      {
        /** 注意：若加了左斜杠则不会继承父路由的path */
        path: '/home/son2',
        meta: {
          title: `嵌套子组件2`,
        },
        component: () => import('../views/About.vue')
      }
    ]
  },
  /** 路由懒加载 */
  {
    path: '/about',
    meta: {
      title: `关于`,
      keepAlive: false
    },
    name: 'About',
    component: () => import('../views/About.vue'),
    children: [
      {
        path: 'son',
        meta: {
          title: `嵌套子组件`,
        },
        component: Son
      }
    ]
  },
]

const router = new VueRouter({
  mode: 'history',
  base: process.env.BASE_URL,
  linkActiveClass: 'routerActive',
  routes
})

router.beforeEach((to, from, next) => {
  console.log(to)
  console.log(from)
  console.log(router)
  console.log("路由前执行")
  /** 获取meta中的title并设置  */
  document.title = to.matched[to.matched.length-1].meta.title;



  next()
})

router.afterEach((to, from) => {
  console.log("路由后执行")
})
export default router
